﻿using Invantive.Data;
using System;
using System.Data;
using System.Data.Common;
using System.Windows.Forms;

namespace Invantive.Bridge.Sample.ExactOnline
{
    class Program
    {
        static void Main(string[] args)
        {
            DbProviderFactory providerFactory = new InvantiveClientFactory();

            using (IDbConnection connection = providerFactory.CreateConnection())
            {
                /* *** REMEMBER TO CHANGE USER NAME, PASSWORD AND COUNTRY CODE TLD (nl, de, be, co.uk, com, fr, es). *** */
                const string EOL_COUNTRY_TLD = "nl";
                const string EOL_USER = "log on code";
                const string EOL_PASSWORD = "pwd";
                const string EOL_TOTP_SECRET = "totp secret code";
                const string EOL_ACCESS_TOKEN = null;
                const string EOL_REFRESH_TOKEN = null;

                //
                // Please note the following:
                //
                // - On initial use, a bundled 30-day trial license is used automatically. Once that license expires, you will need to acquire
                //   either a user license or a partner license. OEM, ISV, white label and VAR licenses are also available.
                //   The location where you have stored the license file can be specified in the connectionstring using 'licensefile=...'.
                // - During the trial period, a bundled client ID will be used. When you build an application using Invantive Bridge, you must configure
                //   a custom client ID per se the Exact Online Partner Agreements which you enter into.
                // - 2 phase authentication is and will not be supported with the headless OAuth provider. It is only supported with the OAuth Windows Forms 
                //   provider and the OAuth Web provider.
                // - When you have any questions, please check http://stackoverflow.com first. Otherwise contact Invantive using phone +31880026599 (preferred) or email
                //   support@invantive.com.
                // - The number of tables varies by the platform. On Exact Online there are approximately 750 tables available. See for instance the view
                //   'select name from systemtables@datadictionary'.
                //
                DbConnectionStringBuilder builder = new DbConnectionStringBuilder()
                { { "provider", "ExactOnlineAll" }
                , { "apiUrl", $"https://start.exactonline.{EOL_COUNTRY_TLD}" }
                };

                if (!string.IsNullOrEmpty(EOL_USER))
                {
                    builder.Add("User ID", EOL_USER);
                    builder.Add("Password", EOL_PASSWORD);
                }

                if (!string.IsNullOrEmpty(EOL_TOTP_SECRET))
                {
                    builder.Add("totp-secret", EOL_TOTP_SECRET);
                }

                if (!string.IsNullOrEmpty(EOL_ACCESS_TOKEN))
                {
                    builder.Add("api-access-token", EOL_ACCESS_TOKEN);
                }

                if (!string.IsNullOrEmpty(EOL_REFRESH_TOKEN))
                {
                    builder.Add("api-refresh-token", EOL_REFRESH_TOKEN);
                }

                connection.Open();

                using (IDbCommand command = connection.CreateCommand())
                {
                    command.CommandText = "select fullname from me";

                    string fullName = Convert.ToString(command.ExecuteScalar());

                    string msg = $"Hello {fullName}!";

                    MessageBox.Show(msg);

                    //Console.WriteLine(msg);
                }
            }
        }
    }
}
