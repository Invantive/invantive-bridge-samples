# Getting Started with Samples for Invantive Bridge and Visual Studio

Invantive Bridge enables the use of Invantive SQL from within Visual Studio and other tools that support ADO.NET providers.

## Quickstart with Visual Studio

For use with Microsoft Visual Studio:

* Make sure you have .NET Framework 4.7 installed.
* Make sure you have .NET standard 2.0 installed by the [Windows installer](https://download.microsoft.com/download/0/F/D/0FD852A4-7EA1-4E2A-983A-0484AC19B92C/dotnet-sdk-2.0.0-win-x64.exe) on [dot.net core download site](https://www.microsoft.com/net/download/core).
* Start Visual Studio 2017.
* Add `https://nuget.invantive.com/nuget` as a source to your NuGet package manager.
* Open the solution file edit the folder `solutions`.
* Change user name / password where applicable in the projects.
* Run a sample project.

## Supported Platforms

The list of supported platforms includes among others:

* CBS.nl
* DB2
* Documentcloud
* Dropbox
* Dynamics CRM
* ECB
* EDIFACT
* Exact Online
* FTP
* Facebook
* Freshdesk
* Gitlab
* Jira
* Kadaster
* Keepass
* Linkedin
* Loket.nl
* MT940
* Magento
* Mail
* Microsoft Graph (Office 365)
* MySQL
* NASA
* Nmbrs
* Open Exchange Rates
* OpenArch
* OpenSpending.nl
* Oracle
* Paypal
* Postgresql
* RDW.nl
* RSS
* SFTP
* SQL Server
* SWIFT
* Salesforce
* Slack
* Snelstart
* Stackexchange
* Stackoverflow
* Teamleader
* Teamviewer
* Teradata
* Twinfield
* Twitter
* UBL
* VirusTotal
* Visma Severa
* XAA
* XAF
* XAS

## Issues

Please report issues and feature requests through the issues.